package madeit.madeit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;


public class MongoConnectionManagerTest{
	
	private static final String RUN_DESCRIPTION = "Run around the Schlachtensee in below 20 minutes. Turn on your running app and send us the result. If you make it, you win new running shoes";
	private static final String RUN_NAME = "Run around the Schlachtensee in under 20 mins";
	private static final String KICKFLIP_DESCRIPTION = "Do a Kickflip down the 10 stair set, take a video and send it to us";
	private static final String KICKFLIP_NAME = "Kickflip a 10 stair";
	private static DBCollection userColl;
	private static DBCollection challengeColl;
	private static final String EMAIL = "email";
	private static final String PASSWORD = "pw";
	private static final String GENDER = "gender";
	private static final String YOB = "111";
	private static final String COUNTRY = "GER";
	private static final String NAME = "name";
	private static final String KONNI_EMAIL = "konni";
	private static final String KONNI_PW= "pw";
	private static String KONNI_ID;
	private static MongoConnectionManager mongoConnectionManager;

	@BeforeClass
	public static void setUp(){
		MadeItServer.configFile = "src/test/resources/server.properties";
		mongoConnectionManager = new MongoConnectionManager();
		userColl = mongoConnectionManager.getUserColl();
		challengeColl = mongoConnectionManager.getChallengeColl();

		setUpUser();
		setUpChallenges();
		
	}
	
	
	private static void setUpChallenges() {
		BasicDBObject list = new BasicDBObject();
		list.append("loc", new float[] {100, 100});
		list.append("name", KICKFLIP_NAME);
		list.append("description", KICKFLIP_DESCRIPTION);
		challengeColl.insert(list);
		
		BasicDBObject list2 = new BasicDBObject();
		list.append("loc", new float[] {100, 100});
		list2.append("name", RUN_NAME);
		list2.append("description", RUN_DESCRIPTION);
		challengeColl.insert(list2);
	}


	private static void setUpUser() {
		BasicDBObject list = new BasicDBObject();
		list.append("email", KONNI_EMAIL);
		list.append("password", KONNI_PW);
		list.append("country", COUNTRY);
		list.append("name", NAME);
		list.append("yob", Integer.parseInt(YOB));
		list.append("gender", GENDER);
		list.append("challengelist", new ArrayList<Integer>());
		userColl.insert(list);
		
		KONNI_ID = ((ObjectId)userColl.findOne().get("_id")).toHexString();
	}


	/**********************************************************************************
	 *           TESTS FOR USERS
	 **********************************************************************************/
	
	
	@Test
	public void testLoginExistingUserWorks() {
		JsonNode result = mongoConnectionManager.login(KONNI_EMAIL, KONNI_PW);
		String email = result.get("email").textValue();
		assertEquals(KONNI_EMAIL, email);
	}
	
	@Test
	public void testLoginGetsHashedUserID() {
		JsonNode result = mongoConnectionManager.login(KONNI_EMAIL, KONNI_PW);
		String userID = result.get("_id").textValue();
		System.out.println("RESULT: " + result.toString());
		assertEquals(KONNI_ID, userID);
	}
	
	@Test
	public void testLoginNotExistingUserFails() {
		JsonNode result = mongoConnectionManager.login("noooot_existing", "pw");
		assertNull(result);
	}
	
	@Test
	public void testRegisterUser(){

		JsonNode firstLoginResult = mongoConnectionManager.login(EMAIL, PASSWORD);
		assertNull(firstLoginResult);

		mongoConnectionManager.register(EMAIL, NAME,  PASSWORD, COUNTRY, YOB, GENDER);
		
		JsonNode loginResult = mongoConnectionManager.login(EMAIL, PASSWORD);
		assertEquals(COUNTRY, loginResult.get("country").textValue());
		assertEquals(EMAIL, loginResult.get("email").textValue());
		assertEquals(Integer.parseInt(YOB), loginResult.get("yob").asInt());
		assertEquals(GENDER, loginResult.get("gender").textValue());

		
		removeUser(EMAIL);
	}

	
	@Test
	public void testAddRemoveChallengeList(){
		mongoConnectionManager.addToChallengeList(KONNI_ID, 111);
		JsonNode userInfo1 = mongoConnectionManager.login(KONNI_EMAIL, KONNI_PW);

		assertEquals(111, userInfo1.get("watchlist").get(0).asInt());

		mongoConnectionManager.removeFromChallengeList(KONNI_ID, 111);

		JsonNode userInfo2 = mongoConnectionManager.login(KONNI_EMAIL, KONNI_PW);
		assertEquals(0, userInfo2.get("challengelist").size());
	}
	
	@Test
	public void testRemoveIdNotInChallengeListDoesntCrash(){
		mongoConnectionManager.addToChallengeList(KONNI_ID, 111);
		JsonNode userInfo1 = mongoConnectionManager.login(KONNI_EMAIL, KONNI_PW);

		assertEquals(111, userInfo1.get("watchlist").get(0).asInt());

		try{
			mongoConnectionManager.removeFromChallengeList(KONNI_ID, 88888);
		} catch (Exception e) {
			fail();
		}

		assertEquals(111, userInfo1.get("watchlist").get(0).asInt());
		
	}
	
	@Test
	public void testCantCreateTwoUsersSameEmail(){
		mongoConnectionManager.register("sameemail", "name", "pw", "GER", "1988", "M");
		JsonNode registerResult = mongoConnectionManager.register("sameemail", "name", "pwo", "ENG", "1990", "F");
		
		removeUser("sameemail");
		assertEquals(registerResult.textValue(), "DUPLICATION");
	}
	
	@AfterClass
	public static void cleanUp(){
		removeUser(KONNI_EMAIL);
		removeChallenges();
	}
	
	/**********************************************************************************
	 *           TESTS FOR CHALLENGES
	 **********************************************************************************/
	
	@Test
	public void testInsertChallenge(){
		
		String challengeName = "newchallenge";
		
		mongoConnectionManager.insertNewChallenge(challengeName, 1, 2, "blablabla");
		assertEquals(3,challengeColl.find().count());
		challengeColl.remove(new BasicDBObject("name", challengeName));
	}
	
	@Test
	public void testGetChallengesInRadius(){
		JsonNode challenges = mongoConnectionManager.getChallengesInRadius(90, 90, 300);
		assertEquals(1,challenges.size());
		assertEquals(KICKFLIP_NAME, challenges.get(0).get("name").textValue());
	}
	
	@Test
	public void testGetChallengesInRadiusCorrectNumber(){
		JsonNode challenges = mongoConnectionManager.getChallengesInRadius(90, 90, 300);
		assertEquals(1,challenges.size());
	}
	
	private static void removeUser(String email){
		userColl.remove(new BasicDBObject("email", email));
	}
	
	private static void removeChallenges(){
		challengeColl.remove(new BasicDBObject("name", KICKFLIP_NAME));
		challengeColl.remove(new BasicDBObject("name", RUN_NAME));
	}
	
}
