package madeit.madeit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.MongoCredential;

public class MongoConnectionManager {

	protected MongoClient mongoClient;
	protected DBCollection userColl;
	protected DBCollection challengeColl;
	protected int mongoPort;
	protected String mongoUser;
	protected String mongoPW;
	protected String mongoHost;
	protected String mongoDbName;
	protected String mongoUserCollectionName;
	protected String mongoChallengeCollectionName;
	protected String mongoAuthDbName;
	private DB db;
	
	private Logger logger = LoggerFactory.getLogger(MongoConnectionManager.class.toString());

	public MongoConnectionManager(){


		Properties prop = new Properties();
		InputStream input = null;


		try {
			input = new FileInputStream(MadeItServer.configFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			prop.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}


		mongoHost = prop.getProperty("mongohost");
		mongoPort = Integer.parseInt(prop.getProperty("mongoport"));
		mongoUser = prop.getProperty("mongouser");
		mongoPW = prop.getProperty("mongopw");
		mongoDbName = prop.getProperty("mongodb");
		mongoAuthDbName = prop.getProperty("mongoauthdb");
		mongoUserCollectionName = prop.getProperty("mongousercollection");
		mongoChallengeCollectionName = prop.getProperty("mongochallengecollection");

		logger.debug("logging on to mongo"); 
		MongoCredential credential = MongoCredential.createCredential(mongoUser, mongoAuthDbName, mongoPW.toCharArray());
		try {
			mongoClient = new MongoClient(new ServerAddress(mongoHost, mongoPort), Arrays.asList(credential));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		db = mongoClient.getDB(mongoDbName);
		userColl = db.getCollection(mongoUserCollectionName);
		challengeColl = db.getCollection(mongoChallengeCollectionName);
		
	}

	
	public JsonNode login(String email, String password){
		BasicDBObject query = new BasicDBObject("email", email);
		query.append("password", password);

		DBObject userResult = userColl.findOne(query);
		if(userResult == null)
			return null;
		
		logger.info("USER RESULT : " + userResult.toString());
		
		ObjectId userID = (ObjectId) userResult.get("_id");
		String userHashID = userID.toHexString();
		
		userResult.removeField("_id");
		userResult.put("_id", userHashID);
		userResult.removeField("password");
		ObjectMapper mapper = new ObjectMapper();
		return mapper.valueToTree(userResult);
	}

	
	public boolean insertNewChallenge(String name, float x, float y, String description){
		if(name == null || description == null) return false;
		
		float[] loc = {x,y};
		
		BasicDBObject list = new BasicDBObject();
		list.append("name", name);
		list.append("description", description);
		list.append("loc", loc);
		
		try{
			challengeColl.insert(list);
		} catch (Exception e){
			logger.debug("Exception when inserting Challenge\n", e.toString());
			return false;
		}
		return true;
	}
	
	public JsonNode getAllChallenges(){
		DBCursor cursor = challengeColl.find();
		List<JsonNode> resultList = new LinkedList<JsonNode>();
		ObjectMapper mapper = new ObjectMapper();
		
		while(cursor.hasNext()){
			resultList.add(mapper.valueToTree(cursor.next()));
		}
		return mapper.valueToTree(resultList);
	}
	
	public JsonNode getChallengesInRadius(float x, float y, float radius){
		
		LinkedList<Object> circle = new LinkedList<Object>();
        circle.addLast(new float[] { x, y});
        circle.addLast(radius);

        BasicDBObject query = new BasicDBObject("loc", new BasicDBObject("$within", new BasicDBObject("$center", circle)));
		DBCursor cursor = challengeColl.find(query);

		List<JsonNode> resultList = new LinkedList<JsonNode>();
		ObjectMapper mapper = new ObjectMapper();
		while(cursor.hasNext()){
			resultList.add(mapper.valueToTree(cursor.next()));
		}
		return mapper.valueToTree(resultList);
	}
	
	
	public JsonNode register(String email, String name, String password, String country, String yob, String gender){

		if(email == null || name == null || password == null || country == null || yob == null || gender == null){
			return JsonNodeFactory.instance.textNode("EMPTYFIELD");
		}
		
		BasicDBObject list = new BasicDBObject();
		list.append("email", email);
		list.append("password", password);
		list.append("country", country);
		list.append("name", name);
		list.append("yob", Integer.parseInt(yob));
		list.append("gender", gender);
		list.append("challengelist", new ArrayList<Integer>());
		
		try{
			userColl.insert(list);
		} catch (DuplicateKeyException dpke){
			return JsonNodeFactory.instance.textNode("DUPLICATION");
		}

		return login(email, password);
	}
	
	public boolean addToChallengeList(String userid, int challengeid){
		DBObject userdoc = new BasicDBObject("_id", new ObjectId(userid));
		DBObject challengelistdoc = new BasicDBObject("$push", new BasicDBObject("watchlist", challengeid));
		userColl.update(userdoc, challengelistdoc);
		return true;
	}

	public boolean removeFromChallengeList(String userid, int challengeid){
		DBObject userdoc = new BasicDBObject("_id", new ObjectId(userid));
		DBObject challengelistdoc = new BasicDBObject("$pull", new BasicDBObject("watchlist", challengeid));
		userColl.update(userdoc, challengelistdoc);
		return true;
	}



	DBCollection getUserColl() {
		return userColl;
	}
	
	DBCollection getChallengeColl() {
		return challengeColl;
	}

	
}
