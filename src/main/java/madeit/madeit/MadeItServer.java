package madeit.madeit;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class MadeItServer {
	
	public static String configFile;

    public static void main(String[] args) {
    	if(args == null || args.length == 0){
    		System.out.println("please specify config file");
    		System.exit(1);
    	}
    	configFile = args[0];
        SpringApplication.run(MadeItServer.class, args);
    }
    
}