package madeit.madeit;

import java.sql.SQLException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;


@RestController
public class HttpQueryController {
	
	MongoConnectionManager mongoConnectionManager = new MongoConnectionManager();
	
    @RequestMapping("/login")
    public JsonNode login(@RequestParam(value="email") String email, @RequestParam(value="password") String password) throws ClassNotFoundException, SQLException {
    	return mongoConnectionManager.login(email, password);
    }
    
    @RequestMapping("/register")
    public JsonNode register(String email, String  name, String password, String country, String yob, String gender){
    	return mongoConnectionManager.register(email, name, password, country, yob, gender);
    }

    @RequestMapping("/setChallenge")
    public boolean setChallenge(String name, String description, long x, long y){
    	return mongoConnectionManager.insertNewChallenge(name, x, y, description);
    }
    
    @RequestMapping("/getAllChallenges")
    public JsonNode getAllChallenges(){
    	return mongoConnectionManager.getAllChallenges();
    }
    
    @RequestMapping("/getChallengesInRadius")
    public JsonNode getChallengesInRadius(float x, float y, float radius){
    	return mongoConnectionManager.getChallengesInRadius(x, y, radius);
    }
    
    @RequestMapping("/addToChallengeList")
    public boolean addToChallengeList(String userid, int challengeID){
    	return mongoConnectionManager.addToChallengeList(userid, challengeID);
    }
    
    @RequestMapping("/removeFromChallengeList")
    public boolean removeFromChallengeList(String userid, int challengeID){
    	return mongoConnectionManager.removeFromChallengeList(userid, challengeID);
    }
    
}
